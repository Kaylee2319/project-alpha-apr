from django.shortcuts import render, redirect
from .models import Task
from .forms import create_taskform
from django.contrib.auth.decorators import login_required


@login_required
def create_task(request):
    if request.method == "POST":
        form = create_taskform(request.POST)
        if form.is_valid():
            task = form.save()
            task.save()
            return redirect("list_projects")
    else:
        form = create_taskform()
    context = {
        "form": form,
    }
    return render(request, "task_temps/create_task.html", context)


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "tasks_list": tasks,
    }
    return render(request, "task_temps/users_tasks.html", context)
