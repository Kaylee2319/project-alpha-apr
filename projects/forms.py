from django.forms import ModelForm
from .models import Project


class create_projectform(ModelForm):
    class Meta:
        model = Project
        fields = ["name", "description", "owner"]
