from django.shortcuts import render, redirect, get_object_or_404
from .models import Project
from .forms import create_projectform
from django.contrib.auth.decorators import login_required


@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "list_projects": projects,
    }
    return render(request, "projects_temps/project_list.html", context)


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    context = {"project_info": project}
    return render(request, "projects_temps/project.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = create_projectform(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.author = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = create_projectform()
        context = {
            "form": form,
        }
    return render(request, "projects_temps/create_project.html", context)
